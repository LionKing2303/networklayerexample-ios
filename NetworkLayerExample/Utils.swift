//
//  Utils.swift
//  NetworkLayerExample
//
//  Created by Arie Peretz on 13/06/2019.
//  Copyright © 2019 Arie Peretz. All rights reserved.
//

import UIKit

class BlackButton : UIButton {
    override func awakeFromNib() {
        updateUI()
    }
    
    override func prepareForInterfaceBuilder() {
        updateUI()
    }
    
    func updateUI() {
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 1.0
        layer.cornerRadius = 8.0
        setTitleColor(UIColor.white, for: .normal)
        backgroundColor = UIColor.black
    }
}

extension String {
    func parseUrl() -> (scheme : String, domain : String, path : String)? {
        let arrSplitDoubleSlash = self.components(separatedBy: "://")
        if arrSplitDoubleSlash.count > 1 {
            if let strScheme = arrSplitDoubleSlash.first {
                
                let fullUrl = arrSplitDoubleSlash[1]
                
                let arrSplitUrl = fullUrl.components(separatedBy: "/")
                if arrSplitUrl.count == 1 {
                    return (strScheme,fullUrl,"")
                } else {
                    if let strDomain = arrSplitUrl.first {
                        var strPath : String = ""
                        for index in 1..<arrSplitUrl.count {
                            strPath += "/" + arrSplitUrl[index]
                        }
                        
                        return (strScheme,strDomain,strPath)
                    }
                }
                
            }
        }
        return nil
    }
}
