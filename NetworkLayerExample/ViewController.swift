//
//  ViewController.swift
//  NetworkLayerExample
//
//  Created by Arie Peretz on 11/06/2019.
//  Copyright © 2019 Arie Peretz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtUrl: UITextField!
    @IBOutlet weak var segmentMethod: UISegmentedControl!
    @IBOutlet weak var txtKey: UITextField!
    @IBOutlet weak var txtValue: UITextField!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var textviewBody: UITextView!
    @IBOutlet weak var textviewResponse: UITextView!
    
    private var bodyParams : [String:String] = [:] {
        didSet {
            updateUI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    private func setupUI() {
        txtUrl.text = "https://aplist.herokuapp.com/tasks"
        txtUrl.placeholder = "enter url"
        txtKey.placeholder = "key"
        txtValue.placeholder = "value"
        
        textviewBody.layer.borderWidth = 1.0
        textviewBody.layer.borderColor = UIColor.black.cgColor
        
        textviewResponse.layer.borderWidth = 1.0
        textviewResponse.layer.borderColor = UIColor.black.cgColor
        
        updateUI()
    }

    private func updateUI() {
        txtKey.isEnabled = (segmentMethod.selectedSegmentIndex > 0)
        txtValue.isEnabled = (segmentMethod.selectedSegmentIndex > 0)
        btnAdd.isEnabled = (segmentMethod.selectedSegmentIndex > 0)
        btnClear.isEnabled = (segmentMethod.selectedSegmentIndex > 0)
        textviewBody.text = getBodyParamsText()
    }
    
    private func getBodyParamsText() -> String {
        var strText : String = "[\n"
        if (bodyParams.count == 0) {
            strText += "No parameters\n"
        } else {
            bodyParams.forEach { (tuple) in
                strText += String(format: "\t%@ : %@\n", tuple.key, tuple.value)
            }
        }
        strText += "]"
        return strText
    }
    
    @IBAction func methodValueChanged(_ sender: UISegmentedControl) {
        if (sender.selectedSegmentIndex == 0) {
            bodyParams = [:]
            txtKey.text = ""
            txtValue.text = ""
        }
        updateUI()
    }
    
    @IBAction func btnActionAdd(_ sender: Any) {
        if let key = txtKey.text, let value = txtValue.text {
            bodyParams.updateValue(value, forKey: key)
        }
    }
    
    @IBAction func btnActionClear(_ sender: Any) {
        bodyParams = [:]
    }
    
    @IBAction func btnActionSend(_ sender: Any) {
        if let strUrl = txtUrl.text {
            if let tupleParsed = strUrl.parseUrl() {
                textviewResponse.text = "PROCESSING..."
                if let method  = RequestMethod(rawValue: segmentMethod.selectedSegmentIndex) {
                    let request : BaseRequest = BaseRequest(scheme: tupleParsed.scheme, host: tupleParsed.domain, port: nil, path: tupleParsed.path, method: method)
                    
                    NetworkManager.sharedInstance.makeRequest(request: request, withBody: bodyParams) { (strData) in
                        DispatchQueue.main.async {
                            self.textviewResponse.text = strData
                        }
                    }
                }
            } else {
                textviewResponse.text = "INVALID URL"
            }
        }
    }
    
    @IBAction func handleTap(_ sender: Any) {
        txtUrl.resignFirstResponder()
        txtKey.resignFirstResponder()
        txtValue.resignFirstResponder()
    }
}

