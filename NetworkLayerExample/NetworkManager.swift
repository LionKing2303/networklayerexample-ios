//
//  NetworkManager.swift
//  NetworkLayerExample
//
//  Created by Arie Peretz on 11/06/2019.
//  Copyright © 2019 Arie Peretz. All rights reserved.
//

import Foundation

enum RequestMethod : Int {
    case MethodGet = 0
    case MethodPost
    case MethodPut
    case MethodDelete
    
    func getString() -> String {
        switch self {
        case .MethodGet:
            return "GET"
        case .MethodPost:
            return "POST"
        case .MethodPut:
            return "PUT"
        case .MethodDelete:
            return "DELETE"
        }
    }
}

enum ServerSettings : Int {
    case Local = 1
    case Production
    
    func getBaseURLSettings() -> (scheme:String,host:String,port:Int?) {
        switch self {
        case .Local:
            return ("http","localhost",8080)
        case .Production:
            return ("https","aplist.herokuapp.com",nil)
        }
    }
}



class BaseRequest {
    var scheme : String
    var host : String
    var port : Int?
    var path : String
    var method : RequestMethod
    
    init(scheme : String, host : String, port : Int?, path : String, method : RequestMethod) {
        self.scheme = scheme
        self.host = host
        self.port = port
        self.path = path
        self.method = method
    }
}

class NetworkManager {
    static let sharedInstance = NetworkManager()
    
    static let serverSettings : ServerSettings = ServerSettings.Production
    
    
    func makeRequest(request : BaseRequest, withBody bodyParams : [String:String]?, competionBlock : ((String)->Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = request.scheme
        urlComponents.host = request.host
        urlComponents.port = request.port
        urlComponents.path = request.path
        guard let url = urlComponents.url else { fatalError("Could not create URL from components") }
        
        var doRequest = URLRequest(url: url)
        doRequest.httpMethod = request.method.getString()
        var headers = doRequest.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        doRequest.allHTTPHeaderFields = headers
        doRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        
        if (bodyParams != nil) {
            if (bodyParams!.count > 0) {
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: bodyParams!, options: .prettyPrinted)
                    doRequest.httpBody = jsonData
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: doRequest) { (responseData, response, responseError) in
            guard responseError == nil else {
                competionBlock?(responseError!.localizedDescription)
                return
            }
            if let data = responseData {
                let dataString = String(NSString(data: data, encoding: String.Encoding.utf8.rawValue) ?? "")
                competionBlock?(dataString)
                return
            }
            competionBlock?("ERROR")
            return
        }
        task.resume()
    }
}
